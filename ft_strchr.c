/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 14:14:21 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/16 15:25:30 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		i;
	char	*ss;
	char	cr;

	cr = (char)c;
	ss = (char *)s;
	i = 0;
	if (cr == '\0' && ss[i] == '\0')
	{
		ss[i] = '\0';
		return (ss);
	}
	if (cr == '\0' && ss[i] != '\0')
	{
		while (ss[i] != '\0')
			i++;
		return (&ss[i]);
	}
	while (ss[i] != '\0')
	{
		if (ss[i] == cr)
			return (&ss[i]);
		i++;
	}
	return (NULL);
}
