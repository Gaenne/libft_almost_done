/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 17:06:27 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/29 17:06:29 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	char *ss;
	size_t i;

	i = 0;
	ss = (char *) s;
	while ( i < n)
	{
		if ((char)c == *ss)
			return (ss);
		ss++;
		i++;
	}
	return (NULL);
}
