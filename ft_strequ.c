/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 17:49:42 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/17 13:47:48 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	strcmpstr(const char *s1, const char *s2)
{
	int	i;

	i = 0;
	while (s1[i] == s2[i])
	{
		if (s1[i + 1] == '\0' && s2[i + 1] != '\0')
			return (-1);
		if (s2[i + 1] == '\0' && s1[i + 1] != '\0')
			return (1);
		if (s1[i + 1] == '\0' && s2[i + 1] == '\0')
			return (0);
		i++;
	}
	return (s1[i] - s2[i]);
}

int			ft_strequ(char const *s1, char const *s2)
{
	if (strcmpstr(s1, s2) != 0)
		return (0);
	else
		return (1);
}
