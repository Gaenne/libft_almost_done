/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/01 12:53:29 by gsanquer          #+#    #+#             */
/*   Updated: 2018/12/01 12:53:31 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void	best_norm_ever(t_list **list, t_list **new)
{
	(*list)->next = (*new);
	(*list) = (*list)->next;
}

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*list;
	t_list	*temp;
	int		turn;

	turn = 0;
	if (lst == NULL)
		return (NULL);
	while (lst != NULL)
	{
		if (!(new = (t_list *)malloc(sizeof(*new))))
			return (NULL);
		new = f(lst);
		new->next = NULL;
		if (turn == 0)
		{
			temp = new;
			list = new;
		}
		else
			best_norm_ever(&list, &new);
		lst = lst->next;
		turn++;
	}
	return (temp);
}
