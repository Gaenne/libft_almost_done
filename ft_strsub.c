/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 13:50:30 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/17 17:03:45 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*dest;
	int		i;

	if (s == NULL)
		return (NULL);
	if (!(dest = (char *)malloc(sizeof(*dest) * (len + 1))))
		return (NULL);
	i = 0;
	while (len != 0)
	{
		dest[i] = s[start];
		len--;
		start++;
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
