/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 14:06:39 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/16 14:13:40 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	strlendst(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

static int	strlensrc(char const *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

static char	*strncatstr(char *s1, const char *s2, size_t n)
{
	unsigned long	i;
	unsigned long	j;

	j = 0;
	i = 0;
	while (s1[i] != '\0')
		i++;
	while (s2[j] != '\0' && i < (n - 1))
	{
		s1[i] = s2[j];
		i++;
		j++;
	}
	s1[i] = '\0';
	return (s1);
}

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	unsigned long int	len_src;
	unsigned long int	len_dst;

	len_dst = strlendst(dst);
	len_src = strlensrc(src);
	if (size < len_dst)
		return (len_src + size);
	else
	{
		strncatstr(dst, src, size);
		return (len_dst + len_src);
	}
	return (0);
}
