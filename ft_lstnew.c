/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/01 12:52:26 by gsanquer          #+#    #+#             */
/*   Updated: 2018/12/01 12:52:29 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list *new_link;

	if (!(new_link = (t_list *)malloc(sizeof(*new_link))))
		return (NULL);
	if (content)
	{
		if (content_size > 0)
		{
			if(!(new_link->content = (void *)malloc(content_size)))
				return (NULL);
			ft_memcpy(new_link->content, content, content_size);
		}
		else
			new_link->content = (void *)content;
	}
	else 
		new_link->content_size = 0;
	if (content != NULL)
		new_link->content_size = content_size;
	new_link->next = NULL;
	return (new_link);
}
