/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 18:42:29 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/28 18:42:36 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t i;
	unsigned char *desti;
	unsigned char *srci;
	
	desti = (unsigned char *)dest;
	srci = (unsigned char *)src;
	i = 0;
	while (i < n)
	{
		desti[i] = srci[i];
		if (desti[i] == (unsigned char)c)
			return (desti + i + 1);
		i++;
	}
	return (NULL);
}
