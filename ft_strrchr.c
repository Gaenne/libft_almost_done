/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrch.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:28:39 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/16 15:48:34 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*snew;

	snew = (char *)s;
	i = 0;
	if (c == '\0' && snew[i] == '\0')
	{
		snew[i] = '\0';
		return (snew);
	}
	if (c == '\0' && snew[i] != '\0')
	{
		while (snew[i] != '\0')
			i++;
		return (&snew[i]);
	}
	while (snew[i] != '\0')
		i++;
	while (i-- >= 0)
	{
		if (snew[i] == c)
			return (&snew[i]);
	}
	return (NULL);
}
