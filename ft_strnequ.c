/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 17:56:54 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/17 13:49:00 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	strncmpstr(const char *s1, const char *s2, size_t n)
{
	unsigned long int	i;

	i = 0;
	if (n == 0)
		return (0);
	while (s1[i] == s2[i] && i < n)
	{
		if (s1[i + 1] == '\0' && s2[i + 1] != '\0')
			return (-1);
		if (s2[i + 1] == '\0' && s1[i + 1] != '\0')
			return (1);
		if (s1[i + 1] == '\0' && s2[i + 1] == '\0')
			return (0);
		if (i + 1 == n)
			return (0);
		i++;
	}
	return (s1[i] - s2[i]);
}

int			ft_strnequ(char const *s1, char const *s2, size_t n)
{
	if (strncmpstr(s1, s2, n) != 0)
		return (0);
	else
		return (1);
}
