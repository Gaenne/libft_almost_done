/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:52:14 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/16 16:00:28 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	int i;
	int signe;
	size_t nb;
	long max;

	max = 9223372036854775807LL;
	i = 0;
	signe = 0;
	nb = 0;
	while ((str[i] == ' ') || (str[i] == '\n') || (str[i] == '\v') ||
		(str[i] == '\f') || (str[i] == '\r') || (str[i] == '\t'))
		i++;
	if (str[i] == '-')
		signe = 1;
	if ((str[i] == '-') || (str[i] == '+'))
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		nb *= 10;
		nb += str[i] - 48;
		if (signe == 1 && nb > max)
			return (-1);
		else if (signe == -1 && nb > (size_t)(max) + 1)
			return (0);
		i++;
	}
	if (signe == 1)
		return (-nb);
	else
		return (nb);
}
