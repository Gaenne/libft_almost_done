/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/01 12:52:45 by gsanquer          #+#    #+#             */
/*   Updated: 2018/12/01 12:52:47 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list **temp;
	t_list *temp_temp;

	temp = alst;
	while ((*temp) != NULL)
	{
		del((*temp)->content, (*temp)->content_size);
		temp_temp = (*temp);
		(*temp) = (*temp)->next;
		free(temp_temp);
	}
	free(*alst);
	*alst = NULL;
}
