/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 12:50:20 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/19 14:20:57 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_lenword(char const *s, char c)
{
	int i;
	int word;

	i = 0;
	word = 0;
	while (s[i] != '\0')
	{
		if (s[i] != c && (s[i + 1] == '\0' || s[i + 1] == c))
			word++;
		i++;
	}
	return (word);
}

static char	*getlastchar(char const *s, int *pos, char c)
{
	int		i;
	int		x;
	char	*str;

	x = 0;
	i = *pos;
	while (s[*pos] != '\0' && s[*pos] != c)
		*pos = (*pos) + 1;
	if (!(str = (char *)malloc(sizeof(*str) * ((*pos) - i + 1))))
		return (NULL);
	while (i < *pos)
	{
		str[x] = s[i];
		i++;
		x++;
	}
	str[x] = '\0';
	return (str);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**split;
	int		i;
	int		nb_word;

	i = 0;
	nb_word = 0;
	if (!(split = (char **)malloc(sizeof(*split) * (ft_lenword(s, c) + 1))))
		return (NULL);
	while (s[i] != '\0')
	{
		if (s[i] != c)
		{
			split[nb_word] = getlastchar(s, &i, c);
			nb_word++;
		}
		else
			i++;
	}
	split[nb_word] = NULL;
	return (split);
}
