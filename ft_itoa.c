/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 13:58:36 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/27 14:11:40 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	fill_number(int len, long int check, long int nb, char **number)
{
	int i;

	i = 0;
	if ((*number)[0] == '-')
	{
		i++;
		len++;
	}
	check = check / 10;
	while (i < len)
	{
		(*number)[i] = (nb / check) + '0';
		(*number)[i + 1] = '\0';
		nb = nb % check;
		check = check / 10;
		i++;
	}
	(*number)[i] = '\0';
}

static void	get_len(int *len, long int *check, long int nb)
{
	while ((nb / (*check)) >= 1)
	{
		(*check) = (*check) * 10;
		(*len)++;
	}
}

static char	*manage_zero(void)
{
	char *str;

	if (!(str = (char *)malloc(sizeof(*str) * 2)))
		return (NULL);
	str[0] = '0';
	str[1] = '\0';
	return (str);
}

char		*ft_itoa(int n)
{
	int			signe;
	char		*number;
	int			len;
	long int	check;
	long int	nb;

	len = 0;
	signe = 0;
	check = 1;
	nb = n;
	if (n == 0)
		return (manage_zero());
	if (n < 0)
	{
		signe = 1;
		nb = nb * (-1);
	}
	get_len(&len, &check, nb);
	if (!(number = (char *)malloc(sizeof(*number) * (len + signe + 1))))
		return (NULL);
	if (signe == 1)
		number[0] = '-';
	fill_number(len, check, nb, &number);
	return (number);
}
