/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmoce.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 17:03:44 by gsanquer          #+#    #+#             */
/*   Updated: 2018/12/03 12:54:00 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char			*csrc;
	char			*cdest;

	csrc = (char *)src;
	cdest = (char *)dst;
	if (csrc < cdest)
	{
		len--;
		while (len + 1 > 0)
		{
			cdest[len] = csrc[len];
			len--;
		}
	}
	else
		ft_memcpy(cdest, csrc, len);
	return (cdest);
}
