#include <stdio.h>
#include<string.h>
#include<stdlib.h>
#include "ft_strlen.c"
#include "ft_itoa.c"

ft_strsub
// int	main(void)
// {
// 	char const s[]= "c'est qui le patron? c'est moi dis donc";
// 	unsigned int start = 6;
// 	printf("%s\n", ft_strsub(s, start, 6));
// 	return (0);
// }

ft_sqrt
// int main (void)
// {
// 	printf("%d\n", ft_sqrt(9));
// 	return (0);
// }

ft_is_prime
// int	main(void)
// {
// 	int nb = 53;
// 	printf("%d\n", ft_is_prime(nb));
// 	return (0);
// }

ft_striteri
// void	toto(unsigned int n, char *str)
// {
// 	str[n] += 1;
// }
// int main(void)
// {
// 	char s[] = "ABCDEFGH";
// 	ft_striteri(s, toto);
// 	printf("%s\n", s);
// 	return (0);
// } 

ft_memdel
// int	main(void)
// {
// 	int *tata = malloc(sizeof(*tata) * 5);
//     tata[0] = 't';
//       tata[1] = 'o';
//       tata[2] = 't';
//       tata[3] = 'o';
//       tata[5] = '\0';
//       void *tete = (void *)tata;
//     ft_memdel(&tete);
//     printf("%s\n", ((char *)tata ));
// }

ft_strdup
// int 	main(void)
// {
// 	const char s1[] = "";
// 	printf("M :%s\n", ft_strdup(s1));
// 	printf("R :%s\n", strdup(s1));
// 	return (0);
// }

ft_strjoin
// int main(void)
// {
// 	char const s1[] = "rdgsdgsdfgdfsgsdfg";
// 	char const s2[] = "Inchallah";
// 	printf("%s\n", ft_strjoin(s1,s2));
// 	return (0);
// }
ft_strncpy
// int main(void)
// {
// 	char src[] = "ner?";
// 	char dst[10];
// 	size_t len = 6;
// 	printf("Mine :%s\n", ft_strncpy(dst, src, len));
// 	printf("Real :%s\n", strncpy(dst, src, len));
// 	return (0); 
// }

ft_strnequ
// int	main(void)
// {
// 	char const s1[] = "salut c'est toi";
// 	char const s2[] = "salut c'est moi";
// 	size_t n = 0;
// 	printf("%d\n", ft_strnequ(s1, s2, n));
// 	return (0);
// }

ft_strnew
// int	main(void)
// {
// 	size_t size = 10;
// 	ft_strnew(size);
// 	return (0);
// }

ft_strtrim
// int	main(void)
// {
// 	char const s[] = "  \t   \n cela vas-t-il fonctionner     j  \n  5 \t";;
// 	printf("Mine :%s\n", ft_strtrim(s));
// 	return (0);
// }

ft_strnstr
// int	main(void)
// {
// 	const char haystack[]= "12345678foin  sdsdds";
// 	const char needle[]= "foin";
// 	size_t len = 10;
// 	printf("Mine : %s\n", ft_strnstr(haystack, needle, len));
// 	printf("reel : %s\n", strnstr(haystack, needle, len));
// 	return (0);
// }

ft_strcmp
// int main(void)
// {
// 	const char s1[]= "Salut c'est goi.";
// 	const char s2[]= "Salut c'est moi.";
// 	printf("Mine : %d\n", ft_strcmp(s1, s2));
// 	printf("reel : %d\n", strcmp(s1, s2));
// 	return (0);
// }

ft_striter
// void	toto(char *str)
// {
// 	*str += 1;
// }
// int main(void)
// {
// 	char s[] = "ABCDEFGH";
// 	ft_striter(s, toto);
// 	printf("%s\n", s);
// 	return (0);
// }

ft_strncmp
// int main(void)
// {
// 	size_t n = 20;
// 	const char s1[]= "Salut C'est moi.";
// 	const char s2[]= "Salut c'est moi.";
// 	printf("Mine : %d\n", ft_strncmp(s1, s2, n));
// 	printf("reel : %d\n", strncmp(s1, s2, n));
// 	return (0);
// }

ft_strrchr
// int main(void)
// {
// 	char s[] = "dsdgsosdkdsds g dsdssd";
// 	int c = 'g';
// 	printf("Mine :%s\n", ft_strrchr(s, c));
// 	printf("Real :%s\n", strrchr(s, c));
// 	return (0);
// }

ft_memcmp
// int	main(void)
// {
// 	char s1[] = "il etait une fois dans l'kuest.";
// 	char s2[] = "il etait une fois dans l'ouest.";
	
// 	printf("Result reel : %d\n",memcmp(s1, s2, 30));
// 	printf("result mine : %d\n", ft_memcmp(s1, s2, 30));
// 	return (0);
// }

ft_strstr
// int main(void)
// {
// 	char haystack[] = "C'est quelque chose de presque elementaire";
// 	char needle[] = "chose";
// 	printf("Mine :%s\n", ft_strstr(haystack, needle));
// 	printf("Real :%s\n", strstr(haystack, needle));
// 	return (0);
// }

ft_strncat
// int	main(void)
// {
// 	char s1[50] = "";
// 	const char s2[] = "hgfftt hg";
// 	size_t n = 4;
// 	//printf("Real :%s\n", strncat(s1, s2, n));
// 	ft_putstr(strncat(s1, s2, n));
// 	//printf("Mine :%s\n", ft_strncat(s1, s2, n));
// 	ft_putstr(ft_strncat(s1, s2, n));
// 	return (0);
// }

ft_strsplit
// int main (void)
// {
// 	int i = 0;
// 	char const s[] = "    ****let's****try****this**out***   *";
// 	char **result = ft_strsplit(s, '*');
// 	while (result[i] != NULL)
// 	{
// 		printf("%s\n", result[i]);
// 		i++;
// 	}
// 	return(0);
// }

ft_memchr
// int    main(void)
// {
//     char string1[60] = "Taj Mahal is a histori monucment in India.";
//     char *pdest;
//     char *mypdest;

//     pdest = memchr(string1, 'c', 15);
//     if (pdest == NULL)
//     {
//         printf("Mon pdest est null reel\n");
//     }
//     else {
//         *pdest = '\0';
//     }
//     printf( "Result reel: %s|\n ", string1);
//     mypdest = ft_memchr(string1, 'c', 15);
//     if (pdest == NULL)
//     {
//         printf("Mon pdest est null\n");
//     }
//     else {
//         *pdest = '\0';
//     }
//        printf( "Result mine: %s|\n ", string1);
//     return (0);
// }
ft_memmove
// int main(void)
// {
// 	const char src[50] = "dont";
// 	char dst[10];
// 	ft_memmove(dst, src, 5);
// 	printf("mine : %s\n", dst);
// 	const char srci[50] = "dont";
// 	memmove(dst, srci, 5);
// 	printf("reel : %s\n", dst);
// 	return (0);
// }

ft_memset
// int main(void)
// {
// 	char str[10];
// 	ft_memset(str, 'A', -9);
// 	printf("%s\n", str);
// 	char stri[10];
// 	memset(stri, 'A', -9);
// 	printf("%s\n", stri );
// }

ft_strchr
// int	main(void)
// {
// 	char s[] = "";
// 	int c = 'c';
// 	printf("Mine :%s\n", ft_strchr(s, c));
// 	printf("Real :%s\n", strchr(s, c));
// 	return (0);
// }

ft_strcmp	
// int	main(void)
// {
// 	char const s1[] = ".";
// 	char const s2[] = "";
// 	printf("%d\n", ft_strequ(s1, s2));
// 	return (0);
// }


ft_strclr
// int	main(void)
// {
// 	char s[]= "sdfsdfsdfsdfsd";
// 	ft_strclr(s);
// 	return 0;
// }

ft_strcat
// int main(void)
// {
// 	char s1[] = "ce la vsvsdfsdfsdf";
// 	const char s2[] = "et bien sdfsdfsdfsdfsdf";
// 	printf("%s\n", ft_strcat(s1, s2));
// 	printf("%s\n", strcat(s1, s2));
// 	return (0);
// }

ft_strchr

ft_putnbr
// int main(void)
// {
// 	int n = 6423115;
// 	ft_putnbr(n);
// 	return (0);
// }

ft_putstr_fd
int	main(void)
{
	char const s[] = "coucou c'est moi qui focntionne";
	int fd = 1;
	ft_putstr_fd(s, fd);
	return (0);
}

ft_putnbr_fd
// int	main(void)
// {
// 	int n = 2147483647;
// 	int fd = 1;
// 	ft_putnbr_fd(n, fd);
// 	return (0);
// }

ft_putendl_fd
// int main(void)
// {
// 	char s[] = "inchallah cela focntionne";
//  	ft_putendl(s);
// 	return (0);
// }

ft_strmapi
// int	main(void)
// {
// 	char const s[]= "ABCDEFGH";
// 	printf("%s\n", ft_strmapi(s, addOne));
// 	return (0);
// char addOne(unsigned int n, char c) 
// {
// 	return (c +n);
// }
// }

ft_strmap
// int	main(void)
// {
// 	char const s[]= "ABCDEFGH";
// 	printf("%s\n", ft_strmap(s, addOne));
// 	return (0);
// }
ft_memccpy
// int	main(void)
// {
// 	char string1[60] = "Taj Mahal is a histori monument in India.";
//     char buffer[61];
//     char *pdest;
//     char *mypdest;

//     pdest = memccpy( buffer, string1, 'c', 7);
//     if (pdest == NULL)
//     {
//         printf("Mon pdest est null reel\n");
//     }
//     else {
//         *pdest = '\0';
//     }
//     printf( "Result reel: %s\n ", buffer);
//     mypdest =  ft_memccpy(buffer, string1, 'c', 7);
//     if (mypdest == NULL)
//         printf("mon pdest est null\n");
//     else {
//         *mypdest = '\0';
//     }

// 	printf( "Result mine: %s\n ", buffer);
//     return (0);
// }

ft_strlcat
// 	int main(void)
// {
//     char dst[100] = "il etait un debut";
//     const char src[] = "";
//     size_t size = 0;
//     printf("Mine :%lu\n%s\n", ft_strlcat(dst, src, size), dst);
//     printf("Real :%lu\n%s\n", strlcat(dst, src, size), dst);
//     return (0);
// }

ft_strcpy
// int	main(void)
// {
// 	// //ft_strcpy
// 	// char dst[30];
// 	// const char src[] = " il etait une fois une fonction";
// 	// printf("%s\n", ft_strcpy(dst, src));
// 	// printf("%s\n", strcpy(dst, src));
// 	// return (0);
// 	//ft_itoa
// 	int n = 2147483647;
// 	printf("n = ", ft_itoa(n));
// 	return (0);
// }
ft_isprint
// int main(void)
// {
// 	int c = '\n';
// 	printf("M:%d\n", ft_isprint(c));
// 	printf("R:%d\n", isprint(c));
// 	return (0);
// }

ft_strcpy
// int	main(void)
// {
// 	char dst[5];
// 	char src[] = "Il etait une fois dans l'ouest";
// 	printf("dst = %s\n", ft_strcpy(dst, src));
// 	printf("reel=%s\n", strcpy(dst, src));
// 	return (0);
// }

ft_isdigit
// int main(void)
// {
// 	int c = '6';
// 	printf("M:%d\n", ft_isdigit(c));
// 	printf("R:%d\n", isdigit(c));
// 	return (0);
// }

ft_isascii
// int main(void)
// {
// 	int c = '`';
// 	printf("M:%d\n", ft_isascii(c));
// 	printf("R:%d\n", isascii(c));
// 	return (0);
// }
ft_isaplha
// int main(void)
// {
// 	int c = 'f';
// 	printf("M:%d\n", ft_isalpha(c));
// 	printf("R:%d\n", isalpha(c));
// 	return (0);
// }

ft_isalnum
// int main(void)
// {
// 	int c = 'ffds';
// 	printf("M:%d\n", ft_isalnum(c));
// 	printf("R:%d\n", isalnum(c));
// 	return (0);
// }

ft_memalloc
// int	main(void)
// {
// 	size_t size = 7;
// 	ft_memalloc(size);
// 	return (0);
// }

ft_atoi
//int	main(void)
// {
// 	const char str[] = "-21474836478 ";
// 	printf("M:%d\n", ft_atoi(str));
// 	printf("R:%d\n", atoi(str));
// 	return 0;
// }

ft_strdel
// int	main(void)
// {
// 	char **as;
// 	int i = 0;

// 	as = (char **)malloc(sizeof(*as) * (3));
// 	while (i < 3)
// 	{
// 		as[i] = (char *)malloc(sizeof(*as[i])*(5));
// 		as[i][0] = 't';
//         as[i][0] = 'o';
//         as[i][0] = 't';
//         as[i][0] = 'o';
//         as[i][0] = '\0';
//         i++;
// 	}
// 	as[i] = NULL;
// 	i = 0;
// 	while (as[i])
// 	{
// 		printf("%s\n", as[i]);
// 		i++;
// 	}
// 	ft_strdel(as);
// 	return(0);
// }

ft_putendl_fd
// int	main(void)
//  {
//  	char const s[] = " Cela vas-t-il egalement fonctionner";
//  	int fd = 1;
//  	ft_putendl_fd(s, fd);
//  	return (0);
//  }

ft_bzero
// int main(void)
// {
// 	char s[] = "dsfsdfsdf";
// 	size_t n = 4;
// 	ft_bzero(s ,n);
// 	printf("m: %s\n", s);
// 	bzero(s, n);
// 	printf("r: %s", s);
// 	return (0);
// }

ft_memccpy
// int	main(void)
// {
// 	char string1[60] = "Taj Mahal is a histori monucment in India.";
//     char buffer[61];
//     char *pdest;
//     pdest = memccpy( buffer, string1, 'c', 42);
//    *pdest = '\0'; // Comme ça pour remettre a 0 un pointeur IL FAUT FREE AVANT si tu as malloc seulement biensure
//   // pdest =  ft_memccpy(buffer, string1, 'c', 42);
//    printf( "Result: %s|\n ", buffer);
//    printf( "Result: %s|\n ", pdest);
//   // printf("My result = %s|\n", pdest );
//    //printf( "Length: %lu characters\n", strlen(buffer) );
// 	return (0);
// }

ft_memcpy
// int main(void)
// {
// 	char src[]= "chaine a copier";
// 	char dst[03];
// 	size_t n = 5;
// 	printf("Mine :%s\n", ft_memcpy(dst, src, n)); //la reel abort quand dst < src
	
// 	printf("Real :%s\n", memcpy(dst, src, n));
// 	return (0);
// }