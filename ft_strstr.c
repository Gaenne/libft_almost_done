/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 14:17:30 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/16 14:31:20 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(const char *haystack, const char *needle)
{
	int		i;
	int		j;
	char	*haystackr;
	char	*needler;

	haystackr = (char *)haystack;
	needler = (char *)needle;
	i = 0;
	if (needler[i] == '\0')
		return (haystackr);
	while (haystackr[i] != '\0')
	{
		j = 0;
		while (haystackr[i + j] == needler[j])
		{
			if (needler[j + 1] == '\0')
				return (haystackr + i);
			j++;
		}
		i++;
	}
	return (0);
}
