/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 17:40:40 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/19 12:46:21 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_is_first_space(char const *str)
{
	int i;

	i = 0;
	while (str[i] == ' ' || str[i] == '\n' || str[i] == '\t')
		i++;
	return (i);
}

static int	ft_is_last_space(char const *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	i = i - 1;
	while ((str[i] == ' ' || str[i] == '\n' || str[i] == '\t') && i > 0)
		i--;
	return (i);
}

char		*ft_strtrim(char const *s)
{
	char	*trim;
	int		i;
	int		end;
	int		start;

	start = ft_is_first_space(s);
	end = ft_is_last_space(s);
	i = 0;
	if (end - start < 0)
	{
		if (!(trim = (char *)malloc(sizeof(*trim) * 1)))
			return (NULL);
		trim[i] = '\0';
		return (trim);
	}
	if (!(trim = (char *)malloc(sizeof(*trim) * (end - start + 2))))
		return (NULL);
	while (start <= end)
		trim[i++] = s[start++];
	trim[i] = '\0';
	return (trim);
}
