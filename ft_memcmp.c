/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsanquer <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 12:16:48 by gsanquer          #+#    #+#             */
/*   Updated: 2018/11/30 12:16:50 by gsanquer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char const	*strs1;
	unsigned char const	*strs2;
	unsigned int		i;

	i = 0;
	strs1 = s1;
	strs2 = s2;
	if (n == 0)
		return (0);
	while (i < (n - 1) && strs1[i] == strs2[i])
		i++;
	return (strs1[i] - strs2[i]);
}
